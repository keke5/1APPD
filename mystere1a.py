#!/usr/bin/env python3

# mystere1.py : que fait ce programme ?
# devinez puis vérifiez
# on peut utiliser une boucle for au lieu d'une boucle while.
n = int(input('Nombre : '))

s = 0
i = 1
for i in range(1, n+1): # rappel : <= est "plus petit ou égal"
    s += i    # ou s = s + i

print(s)