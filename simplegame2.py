from random import randint # ajoute la fonction randint depuis la bibliothèque "random"
from pathlib import Path

name = input('Bonjour, quel est votre nom ? ') # Demande la saisie du nom
print('Bonjour', name, '!') # Salue l'utilisateur
nombreAlea = randint(1, 100) # Génère un numéro aléatoire
##print("J'ai tiré au sort : ", nombreAlea)
score = 0
print('Essayez de deviner mon nombre entre 1 et 100 ! ') # Donne la règle du jeu
tries = [ ]
# save_game et asknumber = fonction
def save_game(name,score):
    with open('game.txt', 'a') as out:
        out.write(name+","+str(score)+"\score")
#write accepte 1 seul argument ex ( en haut)



def asknumber():
    ok = False
    while not ok:
        guess = input('Votre idée : ')
        ok = guess.isnumeric()
        if not ok:
            print('...')  # Demande la saisie d'un nombre entre 1 et 100
    return int(guess)

## Crée le fichier de scores s'il n'existe pas
if not Path('scores.txt').is_file():  # le fichier n'existe pas
    with open('scores.txt', 'w'):  # crée le fichier
        pass  # ne rien faire

best_scores = {}
with open('scores.txt') as fichier:
    for ligne in fichier:
        player, best = ligne.split(',')
        best_scores[player] = int(best)


while True:
    guess = asknumber()
    tries.append(guess)
    score = score + 1
    if guess == nombreAlea:
        print('Gagné ! En', score, 'coups ! ')
        break
    elif guess > nombreAlea:
         print('Trop grand !')
    else:
         print('Trop petit')

save_game(name,score)
print('Bravo !')
print('Vos essais :', *tries)

if name in best_scores:
    if score < best_scores[name]:
        best_scores[name] = score
        print('Vous avez amélioré votre record !')
    else:
        print('Vous avez deja fait mieux, dommage....')

else:
    best_scores[name] = score
    print('Vous entrez dans le high scores !')

print(best_scores)

with open('scores.txt','w') as scoreligne:
    for player,best in best_scores.items():
        scoreligne.write(player + ',' + str(best) + '\n')
        print(player, best)




