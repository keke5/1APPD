#!/usr/bin/env python3

# mystere1.py : que fait ce programme ?
# devinez puis vérifiez
# chaine de caractère ( ce qui est en vert) avec des guillemets (") et apostrophe (').
n = int(input('Nombre : '))

s = 0
i = 1
while i <= n: # rappel : <= est "plus petit ou égal"
    s += i    # ou s = s + i
    i += 1    # ou i = i + 1

print(s)

